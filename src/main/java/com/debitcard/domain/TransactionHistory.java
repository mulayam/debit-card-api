package com.debitcard.domain;

import java.util.Date;

public class TransactionHistory {

	private int transactionId;
	private Date date;
	private double balance;

	public TransactionHistory(int transactionId, Date date, double balance) {
		super();
		this.transactionId = transactionId;
		this.date = date;
		this.balance = balance;
	}

	public int getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(int transactionId) {
		this.transactionId = transactionId;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public double getBalance() {
		return balance;
	}

	public void setBalance(double balance) {
		this.balance = balance;
	}

}
