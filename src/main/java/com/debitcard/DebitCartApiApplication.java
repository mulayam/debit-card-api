package com.debitcard;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DebitCartApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(DebitCartApiApplication.class, args);
	}

}
