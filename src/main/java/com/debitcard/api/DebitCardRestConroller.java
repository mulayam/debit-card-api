package com.debitcard.api;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.debitcard.domain.ApiResponse;
import com.debitcard.domain.TransactionHistory;
import com.debitcard.exception.InvalidCardNumber;
import com.debitcard.service.CardService;

@RestController
public class DebitCardRestConroller {

	@Autowired
	private CardService cardService;

	@GetMapping("/transactions/{accountNumber}/{startDate}/{endDate}")
	public ApiResponse<List<TransactionHistory>> getTransactionHisotry(@PathVariable String accountNumber,
			@PathVariable String startDate, @PathVariable String endDate) {
		List<TransactionHistory> result = cardService.getTransactionHistory(accountNumber, startDate, endDate);
		ApiResponse<List<TransactionHistory>> data = new ApiResponse<List<TransactionHistory>>("00", "SUCCESS", result);
		return data;
	}

	@PostMapping("/create")
	public ApiResponse<TransactionHistory> createTransaction(@RequestParam String cardNumber,
			@RequestParam double amount) {

		TransactionHistory res = cardService.addNewTransaction(cardNumber, amount);
		return new ApiResponse<TransactionHistory>("00", "NEW RECORD CREATED", res);

	}

	@ExceptionHandler(InvalidCardNumber.class)
	public ApiResponse<String> handleExceptin() {
		return new ApiResponse<String>("99", "ERROR", "INVALID CARD NUMBER");
	}

}
