package com.debitcard.service;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import com.debitcard.domain.TransactionHistory;
import com.debitcard.exception.InvalidCardNumber;

@Service
public class CardService {
	private static Map<String, List<TransactionHistory>> map = new HashMap<>();

	public TransactionHistory addNewTransaction(String cardNumber, double balance) {
		if (map.containsKey(cardNumber)) {
			List<TransactionHistory> list = map.get(cardNumber);
			TransactionHistory e = new TransactionHistory(list.size() + 1, new Date(), balance);
			list.add(e);
			return e;
		} else {
			throw new InvalidCardNumber("Invalid card  number");
		}
	}

	public List<TransactionHistory> getTransactionHistory(String accountNumber, String startDate, String endDate) {
		if (map.containsKey(accountNumber)) {
			List<TransactionHistory> list = map.get(accountNumber);
			Date sd=new Date(startDate);
			Date ed=new Date(endDate);
			
			return list.stream().filter(e -> {
				return e.getDate().after(sd) && e.getDate().before(ed);

			}).collect(Collectors.toList());

		} else {
			throw new InvalidCardNumber("Invalid card  number");
		}

	}
}
